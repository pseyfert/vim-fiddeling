#!/usr/bin/python

import sys
from subprocess import call
import os

# USAGE:
# The idea is to copy and paste error messages from gcc to the opening command of vim.
# The assumption is that vim is called from the same shell as gcc.
# Also works with `grep -InH ...`
# NB: when using LHCb/Gaudi cmake, gcc runs in a different directory than the major `make`.
#     Thus I strip off the leading ../ if that results in a valid filename
#
# e.g.
# [user@host Rec]$ make
# cmake --build build.x86_64+avx2+fma-centos7-gcc62-opt --target all -- 
# [1/6] Building CXX object Tr/PatPV/CMakeFiles/PatPV.dir/src/PVSeed3DTool.cpp.o
# FAILED: /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r1p7/InstallArea/scripts/lcg-g++-6.2.0   -DBOOST_FILESYSTEM_VERSION=3 -DBOOST_SPIRIT_USE_PHOENIX_V3 -DGAUDI_V20_COMPAT -DPACKAGE_NAME=\"PatPV\" -DPACKAGE_VERSION=\"v3r31\" -DPatPV_EXPORTS -D_GNU_SOURCE -Df2cFortran -Dlinux -Dunix -mavx2 -mfma -fmessage-length=0 -pipe -Wall -Wextra -Werror=return-type -pthread -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wsuggest-override -std=c++14 -Wno-deprecated -Wno-empty-body -Wno-unused-local-typedefs -O3 -g -DNDEBUG -fPIC -ITr/PatPV -I../Tr/PatPV -I../Pr/PrKernel -I../Tr/TrackInterfaces -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/vdt/0.3.6/x86_64-centos7-gcc62-opt/include -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/XercesC/3.1.3/x86_64-centos7-gcc62-opt/include -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/cppgsl/b07383ea/x86_64-centos7-gcc62-opt -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/clhep/2.3.4.4/x86_64-centos7-gcc62-opt/include -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/GSL/2.1/x86_64-centos7-gcc62-opt/include -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/rangev3/7c2b10f0/x86_64-centos7-gcc62-opt/include -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/AIDA/3.2.1/x86_64-centos7-gcc62-opt/src/cpp -I/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/tbb/44_20160413/x86_64-centos7-gcc62-opt/include -isystem /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/ROOT/6.10.02/x86_64-centos7-gcc62-opt/include -isystem /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_89/Boost/1.64.0/x86_64-centos7-gcc62-opt/include -I../Tr/TrackKernel -I../Tf/TfKernel -Iinclude -I/home/pseyfert/hack2/Lbcom/InstallArea/x86_64+avx2+fma-centos7-gcc62-opt/include -I/home/pseyfert/hack2/LHCb/InstallArea/x86_64+avx2+fma-centos7-gcc62-opt/include -I/home/pseyfert/hack2/Gaudi/InstallArea/x86_64+avx2+fma-centos7-gcc62-opt/include -MMD -MT Tr/PatPV/CMakeFiles/PatPV.dir/src/PVSeed3DTool.cpp.o -MF Tr/PatPV/CMakeFiles/PatPV.dir/src/PVSeed3DTool.cpp.o.d -o Tr/PatPV/CMakeFiles/PatPV.dir/src/PVSeed3DTool.cpp.o -c ../Tr/PatPV/src/PVSeed3DTool.cpp
# ../Tr/PatPV/src/PVSeed3DTool.cpp: In member function 'virtual std::vector<ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>, ROOT::Math::DefaultCoordinateSystemTag> > PVSeed3DTool::getSeeds(const std::vector<const LHCb::Track*>&, const PrPVTracks&, const XYZPoint&) const':
# ../Tr/PatPV/src/PVSeed3DTool.cpp:335:1: error: 'foo' was not declared in this scope
#  foo
#  ^~~
# make: *** [all] Error 1
# [user@host Rec]$ myvim.py ../Tr/PatPV/src/PVSeed3DTool.cpp:335:1:


fwdargs = []

for arg in sys.argv[1:]: # the leading argument will be vim_from_compilererror.py (the program name)
  if arg[0]=="-": # options will be forwarded. NB: heuristic, may fail if you use other arguments than I do
    fwdargs.append(arg)
  else:
    # expected patterns are
    # filename
    # filename:
    # filename:sometext
    # filename:sometext:
    # filename:linenumber
    # filename:linenumber:
    # filename:linenumber:sometext
    # filename:linenumber:columnumber
    # filename:linenumber:columnumber:
    # filename:linenumber:columnumber:sometext

    thelist = arg.split(":")
    thelist = [ x for x in thelist if x != '' ] # remove empty strings     

    # check if file exist and then use it as filename
    if os.path.exists(thelist[0]):
      fwdargs.append(thelist[0])         # forward filename
    # file doesn't exist, often a leading "../" is the problem
    else:
      try:
        if thelist[0][:3] == "../" and os.path.exists(thelist[0][3:]):
          fwdargs.append(thelist[0][3:])         # forward filename
        else:
          fwdargs.append(thelist[0])         # logic doesn't work, just forward what should be the filename
      except:
        fwdargs.append(thelist[0])         # logic doesn't work, just forward what should be the filename
    try:
      # is there a colon? something after the colon? is it a number?
      s = int(thelist[1])
    except:
      # one either there is nothing, or it isn't a number, stop here, ignore the rest of the argument
      pass
    else:
      if 3<=len(thelist): # two numbers, do the cumbersome call. no need to escape stuff with "
        try:
          # check if there is a second number (column)
          s = int(thelist[2])
        except:
          # whatever came after the second colon is not a number ignore it and use just the line number
          fwdargs.append("+"+thelist[1])
        else:
          fwdargs.append("+normal "+thelist[1]+"G"+thelist[2]+"|")
      if 2==len(thelist):
        # only one additional number, this will be the line
        fwdargs.append("+"+thelist[1])

print ["vim"]+fwdargs

call(['vim']+fwdargs)

