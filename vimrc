if has('python')
  map <C-I> :pyf SOMETHINGLB/clang-format.py<cr>
  imap <C-I> <c-o>:pyf <path-to-this-file>/clang-format.py<cr>
elseif has('python3')
  map <C-I> :py3f <path-to-this-file>/clang-format.py<cr>
  imap <C-I> <c-o>:py3f <path-to-this-file>/clang-format.py<cr>
endif

