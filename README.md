# ideas

 * advertise `forusers` file to users and let them copy&paste the content
 * central `vimrc` file stays maintained and keeps getting new features (if they come)
 * per-feature files (`clang-format.py`) for actual functionality of individual plugins

# what's missing

 * configurability: should user configure which key a feature goes to?
 * also configurability: kill switch so user can deselect features they don't like
 * YCM

# LICENSE disclaimer

 * The original `clang-format.py` file (from which the one present here is
   derived) comes with clang-format itself and is licensed under the University
   of Illinois/NCSA Open Source License. This is compatible with GPL, thus the
   publication within the present project under GPL.
   The original's copyright holders are:

    LLVM Team
    University of Illinois at Urbana-Champaign
    http://llvm.org

Once I draw a logo, the following sources are relevant

 * VIM logo usage: The VIM logo is under GPL. The original author of VIM is
    Bram Moolenaar
    http://www.vim.org

 * other files
    LHCb
    http://lhcb.cern
